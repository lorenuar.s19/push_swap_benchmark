sub custom_logexp {

	# print "base : $_[0] | val : $_[1]\n";
	my $base = $_[0];
	my $val = $_[1];
	if ($val <= 1 || $base <= 1)
	{
		return (0);
	}

	return 1 + custom_logexp( $base, $val / $base );
}


# for ($base = 1;$base < 2; $base += 0.01)
# {
# 	for ($div = 0.01; $div < 10000; $div += 0.1)
# 	{
# 		$res25 = custom_logexp($base, 25 / $div);
# 		$res100 = custom_logexp($base, 100 / $div);
# 		$res500 = custom_logexp($base, 500 / $div);
# 		# print "base $base | div $div | res25 $res25 | res100 $res100 | res500 $res500                     \r";
# 		if ($res100 == 5 && $res500 == 11 && $res25 >= 1)
# 		{
# 			$res10 = custom_logexp($base, 10 / $div);
# 			print "base $base \t| div $div \t| res10 $res10 \t| res25 $res25 \t| res100 $res100 \t| res500 $res500\n";
# 		}
# 	}
# }

# for ($nbr = 1; $nbr <= 1000; $nbr++)
# {
# 	$res = custom_logexp(1.3, $nbr / 32);
# 	if ($nbr % 25 == 0)
# 	{
# 		print "$nbr : $res\n";
# 	}
# }


# open (OUT, "+>loga.csv") or die "DEAD";

# print OUT "nbr , base , result\n";
# for ($base = 1; $base <= 5; $base += 0.001)
# {
# 	for ($nbr = 5; $nbr <= 50; $nbr++)
# 	{
# 		$res = custom_logexp($base, $nbr);
# 		print OUT "$nbr , $base , $res\n";
# 	}
# }

# close (OUT);
