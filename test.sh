#!/bin/bash
if [[ $# -ne 1 ]]
then
	printf "Usage:\n$0 [number of elements]"
	exit 1
fi

if [[ $1 -lt 50 ]]
then
	VERBOSE='-v'
else
	VERBOSE='-vs'
fi
# VERBOSE=''

export NUM=$1

# echo FAILED InputSize InstrCount > log.csv
while true
do
	# NUM=$(( 1 + ($RANDOM % $1) ))
	# LIST=$(perl -e "use List::Util 'shuffle'; my @out = (shuffle 1..$NUM)[0..$NUM]; print \"@out\"")

	LIST=$(perl <(cat <<'END_OF_PERL'

	use List::Util 'shuffle';
	$Max = $ENV{'NUM'};
	$RndMax = $Max * 10;
	$index=0;
	for($i = 0; $i < $Max; $i++)
	{
		$tmp = 1 + int(rand() * $RndMax);
		$index += $tmp;
		if ((rand() * 5) % 5 == 0) {
			push(@numbers, -$index);
		} else {
			push(@numbers, $index);
		}
	}
	@numbers = shuffle(@numbers);
	print "@numbers";

END_OF_PERL
))

	echo "LIST = "${LIST[@]}
	if [[ -z "${LIST[@]}" ]]
	then
		echo LIST IS EMPTY
		exit 1;
	fi
	FAIL=0
	# TEST=$(./sub_push_swap/push_swap "${LIST[@]}" | wc -l)
	if [[ $TEST -gt 1500 ]]
	then
		# printf "\033[1;31m\nFAIL\033[33m\n"
		FAIL=1
	fi
	# set -v
	./sub_push_swap/push_swap $VERBOSE "${LIST[@]}"
	# set +v
	# ./sub_push_swap/push_swap "${LIST[@]}" | ./sub_checker/checker "${LIST[@]}"
	# ./sub_push_swap/push_swap "${LIST[@]}" | wc -l

	if [[ -z $TEST ]]; then TEST=NaN; fi
	echo InputSize $NUM InstrCount $TEST
	echo $FAIL $NUM $TEST >> log.csv

	# read -p '. . .'
	# if [[ $FAIL -eq 1 ]]; then echo $FAIL ; fi
	break ;
done
