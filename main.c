#include "./sub_libcommon/includes/libcommon.h"
#include "./sub_libutils/includes/libutils.h"

int	bench_check_sorted(int num_test, t_stack **sta)
{
	int	i;

	i = num_test;
	while (i--)
	{
		stack_rev_rotate(sta);
	}
}

int	create_stack(t_stack **sta, int size)
{
	int	i;

	i = 0;
	while (i < size)
	{
		if (!stack_push_back_data(sta, i++))
		{
			return (error_put(1, "ERROR STACK PUSH BACK"));
		}
	}
	return (0);
}

void	free_stack(t_stack **sta)
{
	while (sta && *sta)
	{
		stack_pop(sta);
	}
}

int	benchmark(int num_tests, int stack_size)
{
	t_stack	*sta;

	sta = NULL;
	if (create_stack(&sta, stack_size))
	{
		return (error_put(1, "ERROR CREATE STACK"));
	}
	if (bench_check_sorted(num_tests, &sta))
	{
		return (error_put(1, " ERROR bench_check_sorted"));
	}
	free_stack(&sta);
	return (0);
}

int	main(int argc, char *argv[])
{
	int	num_tests;
	int	stack_size;

	num_tests = 25;
	stack_size = 50;
	if (argc >= 2)
	{
		num_tests = str_to_nbr(argv[1]);
	}
	if (argc >= 3)
	{
		stack_size = str_to_nbr(argv[2]);
	}
	// printf("%d check_sorted() with stack size of %d\n", num_tests, stack_size);
	if (benchmark(num_tests, stack_size))
	{
		return (error_put(1, "Error from benchmark"));
	}
		return (0);
}
