#include <stdio.h>

#include "./sub_libutils/includes/libutils.h"


#define K 4

// k must be power of 2
size_t	get_digit(size_t num, int n)
{
	return (((num >> (n * K)) & ((1 << K) - 1)));
}

#if 0

int	main(int argc, char *argv[])
{
	size_t	num;

	int base = 1 << K;
	printf("base %d\n", base);

	if (argc == 2)
	{
		num = (size_t)strtol(argv[1], NULL, base);
	}
	size_t	maxlen = num_len_unsigned(num, base);
	size_t	i = 0;
	printf("maxlen %ld\nNUM %lx\n", maxlen, num);
	while (i < maxlen)
	{
		printf("Comp i %ld | %lx\n", i, get_digit(num, i));
		i++;
	}
	i = maxlen;
	printf("REC ");
	while (i > 0)
	{
		i--;
		printf("%lx ", get_digit(num, i));
	}
	printf("\nNUM %lx\n", num);
}

#endif
