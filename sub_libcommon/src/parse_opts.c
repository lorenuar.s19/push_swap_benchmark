/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_opts.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/24 15:08:43 by lorenuar          #+#    #+#             */
/*   Updated: 2021/04/04 00:51:45 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libcommon.h"

int	parse_opts(char argc, char *argv[])
{
	int	i;

	i = 0;
	while (i < argc && argv && argv[i])
	{
		if (strncmp("-v", argv[i], 3) == 0)
			return (1);
		if (strncmp("-vs", argv[i], 4) == 0)
			return (2);
		i++;
	}
	return (0);
}
