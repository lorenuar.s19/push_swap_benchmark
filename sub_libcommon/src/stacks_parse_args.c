/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stacks_parse_args.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/17 15:54:14 by lorenuar          #+#    #+#             */
/*   Updated: 2021/04/04 00:49:37 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libcommon.h"

int	stacks_parse_args(int argc, char *argv[], t_stack **a)
{
	if (argc == 2)
	{
		*a = stack_from_str(argv[1], a);
		if (!a)
			return (error_put(1, E_SPA0));
	}
	else if (argc > 2)
	{
		*a = stack_from_args(argc, argv, a);
		if (!a)
			return (error_put(1, E_SPA1));
	}
	if (a && !*a)
		return (error_put(1, E_SPS0));
	return (0);
}
