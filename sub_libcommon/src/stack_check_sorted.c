/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_check_sorted.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/19 14:07:08 by lorenuar          #+#    #+#             */
/*   Updated: 2021/04/04 00:51:52 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libcommon.h"

int	stack_check_sorted(t_stack *sta)
{
	int		diff;

	if (!sta)
		return (error_put(1, E_SCS0));
	diff = 0;
	while (sta && sta->next)
	{
		if (sta->data > sta->next->data)
		{
			diff++;
		}
		sta = sta->next;
	}
	if (diff)
	{
		return (diff);
	}
	return (diff);
}
