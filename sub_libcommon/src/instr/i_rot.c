/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   i_rot.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/17 22:06:05 by lorenuar          #+#    #+#             */
/*   Updated: 2021/03/19 16:01:58 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libcommon.h"

int	i_rot_a(t_stack **sta, t_stack **stb)
{
	(void)stb;
	if (!sta || !*sta || !(*sta)->next)
		return (0);
	return (stack_rotate(sta));
}

int	i_rot_b(t_stack **sta, t_stack **stb)
{
	(void)sta;
	if (!stb || !*stb || !(*stb)->next)
		return (0);
	return (stack_rotate(stb));
}

int	i_rot_both(t_stack **sta, t_stack **stb)
{
	return (i_rot_a(sta, stb) || i_rot_b(sta, stb));
}
