/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_check_valid.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/19 14:07:08 by lorenuar          #+#    #+#             */
/*   Updated: 2021/04/04 00:50:24 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libcommon.h"

int	stack_check_valid(t_stack *sta)
{
	int		dup;
	t_stack	*top;
	t_stack	*tmp;

	if (!sta)
		return (error_put(1, E_SCV0));
	top = sta;
	tmp = top;
	while (sta)
	{
		tmp = sta;
		dup = 0;
		while (sta && tmp)
		{
			if (sta->data > INT_MAX || sta->data < INT_MIN)
				return (error_put(2, E_SCV1));
			if (sta->data == tmp->data)
				dup++;
			tmp = tmp->next;
		}
		if (dup != 1)
			return (1);
		sta = sta->next;
	}
	return (0);
}
