/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/10 13:38:11 by lorenuar          #+#    #+#             */
/*   Updated: 2021/04/13 23:56:20 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include "libutils.h"
# include "libcommon.h"

# include <debug_utils.h>		//TODO Remove before push
# include "debug.h"				//TODO Remove before push

typedef struct s_range
{
	ssize_t		min;
	ssize_t		max;
}				t_range;

typedef struct s_neighbors
{
	ssize_t		befor;
	ssize_t		after;
}				t_ngbr;

typedef struct s_index_table
{
	size_t		index;
	ssize_t		number;
	ssize_t		n_instr;
	t_ngbr		ngbr;
}				t_itab;

typedef struct s_sorter_data
{
	t_stack		**sta;
	t_stack		**stb;
	t_range		*range_table;
	t_itab		*unsorted;
	t_itab		*sorted;
	ssize_t		range_max;
	ssize_t		min;
	ssize_t		max;
	size_t		i_max;
	size_t		i_min;
	size_t		siz_a;
	size_t		siz_b;
	size_t		siz_itab;
}				t_sdat;

typedef struct s_index_table_data
{
	size_t		i;
	ssize_t		index;
	ssize_t		next;
	int			init_dir;
	int			dir;
}	t_itabdat;

void	print_instr(int instr);
int		generate_instr(t_stack **a, t_stack **b, int verbose);

void	free_sorter_data(t_sdat *sd);

t_range	*create_range_table(t_sdat *sd);
int		setup_index_tables(t_sdat *sd);
int		populate_index_table(t_sdat *sd);
int		create_sorted_table(t_sdat *sd);
int		compute_neighbors(t_sdat *sd);
int		compute_instr(t_sdat *sd);

/*
** Errors
*/

# define E_PS0 "push_swap : error from parse_args()"
# define E_PS1 "push_swap : input is invalid"
# define E_PS2 "push_swap : non zero exit code from generate_instr()"
# define E_PA0 "parse_args : error from stack_from_str()"
# define E_PA1 "parse_args : error from stack_from_args()"
# define E_CRT0 "create_range_table : NULL pointer from malloc()"
# define E_CRT1 "create_range_table : Trying to divide by ZERO"
# define E_ICV0 "itable_check_valid : found unused index"
# define E_SIT0 "setup_index_table : error from create_index_table()"
# define E_SIT1 "setup_index_table : error from get_min_max()"
# define E_SIT2 "setup_index_table : error from populate_index_table()"
# define E_SIT3 "setup_index_table : error from fill_numbers_from_stack()"
# define E_SIT4 "setup_index_table : error from create_index_table()"
# define E_SIT5 "setup_index_table : error from create_sorted_table()"
# define E_GI0 "generate_instr : error from init_sorter_data()"
# define E_GI1 "generate_instr : error from compute_instr()"
# define E_ISD0 "init_sorter_data : error from create_range_table()"
# define E_ISD1 "init_sorter_data : error from setup_index_tables()"

#endif
