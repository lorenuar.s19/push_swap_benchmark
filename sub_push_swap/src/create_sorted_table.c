/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_sorted_table.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/12 16:41:31 by lorenuar          #+#    #+#             */
/*   Updated: 2021/04/13 23:59:25 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	create_sorted_table(t_sdat *sd)
{
	size_t	index;
	size_t	i;

	i = 0;
	while (i < sd->siz_a)
	{
		index = sd->unsorted[i].index;
		sd->sorted[index].index = index;
		sd->sorted[index].number = sd->unsorted[i].number;
		i++;
	}
	i = 0;
	return (0);
}
