/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   compute_instr.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/12 20:56:04 by lorenuar          #+#    #+#             */
/*   Updated: 2021/04/13 22:09:51 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	print_instr_table(t_sdat *sd)
{
	size_t	i;

	i = 0;
	printf("=== Compute instr  === size %4ld ===\n", sd->siz_itab);
	while (i < sd->siz_itab)
	{
		printf("[i %4ld] | n_instr %4ld | number %4ld | unsorted %4ld\n",
			i, sd->sorted[i].n_instr,
			sd->sorted[i].number, sd->unsorted[i].number);
		i++;
	}
}

int	compute_instr(t_sdat *sd)
{
	size_t	i;

	i = 0;
	while (i < sd->siz_itab)
	{
		sd->sorted[i].n_instr = 0;
		i++;
	}
	print_instr_table(sd);


	return (0);
}
