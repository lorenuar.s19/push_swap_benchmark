/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setup_index_tables.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/27 23:09:25 by lorenuar          #+#    #+#             */
/*   Updated: 2021/04/13 21:17:16 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	fill_numbers_from_stack(t_sdat *sd)
{
	t_stack	*sta;
	size_t	i;

	if (!sd->sta || !*(sd->sta))
		return (1);
	sta = *(sd->sta);
	i = 0;
	while (sta && i < sd->siz_itab)
	{
		sd->unsorted[i].number = sta->data;
		sta = sta->next;
		i++;
	}
	return (0);
}

static int 	create_index_table(t_itab **itab, size_t siz_itab)
{
	size_t	i;

	if (!itab)
		return (1);
	(*itab) = malloc(siz_itab * sizeof(t_itab));
	i = 0;
	while (i < siz_itab)
	{
		(*itab)[i].index = -1;
		(*itab)[i].number = 0;
		(*itab)[i].ngbr = (t_ngbr){0, 0};
		i++;
	}
	return (0);
}

static int	get_min_max(t_sdat *sd)
{
	t_stack	*sta;
	size_t	i;

	if (!sd->sta || !*(sd->sta))
		return (1);
	sta = *(sd->sta);
	sd->max = INT_MIN;
	sd->min = INT_MAX;
	i = 0;
	while (sta)
	{
		if (sta->data > sd->max)
		{
			sd->i_max = i;
			sd->max = sta->data;
		}
		if (sta->data < sd->min)
		{
			sd->i_min = i;
			sd->min = sta->data;
		}
		sta = sta->next;
		i++;
	}
	return (0);
}

int	setup_index_tables(t_sdat *sd)
{
	if (create_index_table(&(sd->unsorted), sd->siz_itab))
		return (error_put(1, E_SIT0));
	if (get_min_max(sd))
		return (error_put(1, E_SIT1));
	if (fill_numbers_from_stack(sd))
		return (error_put(1, E_SIT3));
	if (populate_index_table(sd))
		return (error_put(1, E_SIT2));
	if (create_index_table(&(sd->sorted), sd->siz_itab))
		return (error_put(1, E_SIT4));
	if (create_sorted_table(sd))
		return (error_put(1, E_SIT5));
	return (0);
}
