/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   generate_instr.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/22 19:46:15 by lorenuar          #+#    #+#             */
/*   Updated: 2021/04/13 23:18:33 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	exec_print_instr(t_stack **sta, t_stack **stb, int instr,
								int verbose)
{
	if (!verbose)
	{
		print_instr(instr);
	}
	return (exec_instr(sta, stb, instr, verbose));
}

void	display_range_table(t_sdat *sd)
{
	int	i;

	i = 0;
	ft_printf("=== RANGE TABLE === <%p> Size %ld ===\n",
		sd->range_table, sd->range_max);
	while (i < sd->range_max)
	{
		printf("[i %d] min %d max %d\n",
			i, sd->range_table[i].min, sd->range_table[i].max);
		i++;
	}
}

void	display_index_tables(t_sdat *sd)
{
	size_t	i;

	i = 0;
	printf("=== Unsorted table === size %4ld ===\n", sd->siz_a);
	while (i < sd->siz_a)
	{
		printf("[i %4ld] index %4ld | number %4ld | NGBR Be %4ld Af %4ld\n",
			i, sd->unsorted[i].index, sd->unsorted[i].number,
			sd->unsorted[i].ngbr.befor, sd->unsorted[i].ngbr.after);
		i++;
	}
	i = 0;
	printf("===  Sorted table  === size %4ld ===\n", sd->siz_a);
	while (i < sd->siz_a)
	{
		printf("[i %4ld] index %4ld | number %4ld\n",
			i, sd->sorted[i].index, sd->sorted[i].number);
		i++;
	}
}

static int	init_sorter_data(t_sdat *sd)
{
	if (!create_range_table(sd))
	{
		return (error_put(1, E_ISD0));
	}
	sd->siz_itab = sd->siz_a;
	if (setup_index_tables(sd))
	{
		return (error_put(1, E_ISD1));
	}
	return (0);
}

int	generate_instr(t_stack **sta, t_stack **stb, int verbose)
{
	t_sdat	sd;

	sd = (t_sdat){sta, stb, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	if (init_sorter_data(&sd))
	{
		free_sorter_data(&sd);
		return (error_put(1, E_GI0));
	}
	if (verbose)
	{
		display_range_table(&sd);
		display_index_tables(&sd);
	}
	if (compute_instr(&sd))
	{
		free_sorter_data(&sd);
		return (error_put(1, E_GI1));
	}
	free_sorter_data(&sd);
	return (0);
}
