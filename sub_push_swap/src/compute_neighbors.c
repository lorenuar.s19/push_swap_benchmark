/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   compute_neighbors.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/13 23:51:12 by lorenuar          #+#    #+#             */
/*   Updated: 2021/04/14 00:27:01 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static	int	setup_neighbors(t_sdat *sd, t_ngbr *ngbr, ssize_t index)
{
	ssize_t	tmp;
	ssize_t	tmp_index;
BM(++++++++++++++++++++++++++++++++++++++++++++++++++++)
DE(index)
	tmp = index - 1;
	if (tmp < 0)
	{
		tmp = sd->siz_itab - 1;
	}
DM(Before, tmp);
DE(sd->unsorted[tmp].index);
	tmp_index = (ssize_t)sd->unsorted[tmp].index + 1;
DE(tmp_index)
	ngbr->befor = tmp_index - index;
DE(ngbr->befor);
	tmp = index + 1;
	if (tmp > sd->siz_itab - 1)
	{
		tmp = 0;
	}
DM(After, tmp);
DE(sd->unsorted[tmp].index);
	tmp_index = (ssize_t)sd->unsorted[tmp].index + 1;
DE(tmp_index)
	ngbr->after = tmp_index - index;
DE(ngbr->after)
	return (0);
}

int	compute_neighbors(t_sdat *sd)
{
	size_t i;

	i = 0;
	while (i < sd->siz_itab)
	{
		setup_neighbors(sd, &(sd->unsorted[i].ngbr), i + 1);
		printf("[i %4ld] index %4ld | number %4ld | NGBR Be %4ld Af %4ld\n",
			i, sd->unsorted[i].index, sd->unsorted[i].number,
			sd->unsorted[i].ngbr.befor, sd->unsorted[i].ngbr.after);
		i++;
	}
}
