/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   populate_index_table.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/10 00:15:54 by lorenuar          #+#    #+#             */
/*   Updated: 2021/04/14 00:00:29 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	itable_check_valid(t_sdat *sd)
{
	int	i;

	i = 0;
	while (i < sd->siz_itab)
	{
		if (sd->unsorted[i].index == -1)
		{
			return (error_put(1, E_ICV0));
		}
		i++;
	}
	return (0);
}

static ssize_t	find_next_number(t_sdat *sd, ssize_t curr_max)
{
	ssize_t	temp;
	ssize_t	local_max;
	size_t	i;

	i = 0;
	local_max = INT_MIN;
	while (i < sd->siz_a)
	{
		temp = sd->unsorted[i].number;
		if (local_max <= temp && temp < curr_max)
		{
			local_max = temp;
		}
		i++;
	}
	return (local_max);
}

static int	sub_populate_index_table(t_sdat *sd, t_itabdat *dat)
{
	while (dat->dir != 0)
	{
		if (dat->i >= 0 && dat->i < sd->siz_a
			&& sd->unsorted[dat->i].number == dat->next)
		{
			sd->unsorted[dat->i].index = dat->index;
			dat->index--;
			dat->dir = 0;
		}
		if ((dat->init_dir == 1 && dat->i > sd->siz_a - 1)
			|| (dat->init_dir == -1 && dat->i <= 0))
		{
			dat->i = sd->i_max;
			dat->dir = -dat->dir;
		}
		if ((dat->init_dir == -1 && dat->i > sd->siz_a - 1)
			|| (dat->init_dir == 1 && dat->i <= 0))
		{
			dat->dir = 0;
		}
		dat->i += dat->dir;
	}
}

int	populate_index_table(t_sdat *sd)
{
	t_itabdat	dat;

	dat = (t_itabdat){sd->i_max, sd->siz_a - 1, sd->max, 0, 0};
	dat.dir = 0;
	if (sd->i_max > sd->siz_a / 2)
	{
		dat.init_dir = 1;
	}
	else
	{
		dat.init_dir = -1;
	}
	while (dat.next >= sd->min && dat.index >= 0)
	{
		dat.i = sd->i_max;
		dat.dir = dat.init_dir;
		sub_populate_index_table(sd, &dat);
		dat.next = find_next_number(sd, dat.next);
	}
	compute_neighbors(sd);
	return (itable_check_valid(sd));
}
