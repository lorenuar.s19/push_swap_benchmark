#!/bin/bash

gcc -I sub_libcommon/includes -I sub_libutils/includes  main.c sub_libcommon/libcommon.a sub_libutils/libutils.a -o bench

# exit 0

OUTPUT_FILE=result_rev_rotate.csv

NUM_INC=1000000
SIZ_INC=250


echo 'NUM(tests), SIZE(input), TIME(s)' > $OUTPUT_FILE

NUM=$NUM_INC
while [[ $NUM -lt 10000001 ]]
do
SIZE=$SIZ_INC
	while [[ $SIZE -lt 1001 ]]
	do
		printf "%d , %d , " $NUM $SIZE >> $OUTPUT_FILE
		(TIMEFORMAT='%8R' ;time ./bench "$NUM" "$SIZE" ) >> $OUTPUT_FILE 2>&1
		SIZE=$(( $SIZE + $SIZ_INC))
	done
		NUM=$(( $NUM + $NUM_INC ))
done
