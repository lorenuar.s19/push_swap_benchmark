/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/15 17:03:30 by lorenuar          #+#    #+#             */
/*   Updated: 2021/04/03 21:39:27 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

static int	sub_checker_loop(t_stack **a, t_stack **b, int verbose)
{
	int		ret;
	int		instr;

	while (1)
	{
		instr = read_instrs(0);
		if (instr == I_NVALID)
			break ;
		ret = exec_instr(a, b, instr, verbose);
		if (ret)
			return (error_put(ret, E_SCL0));
	}
	return (0);
}

static int	sub_checker_end(t_stack **a, t_stack **b)
{
	if (*b || stack_check_sorted(*a))
	{
		stacks_free_all(a, b);
		put_str("KO\n");
		return (1);
	}
	stacks_free_all(a, b);
	put_str("OK\n");
	return (0);
}

static int	checker(int argc, char *argv[], int verbose)
{
	t_stack	*a;
	t_stack	*b;

	a = NULL;
	b = NULL;
	if (stacks_parse_args(argc, argv, &a))
	{
		stacks_free_all(&a, &b);
		return (error_put(1, E_C0));
	}
	if (stack_check_valid(a))
		return (error_put(1, E_C1));
	print_stacks(a, b, I_NVALID, verbose);
	if (sub_checker_loop(&a, &b, verbose))
	{
		stacks_free_all(&a, &b);
		return (1);
	}
	return (sub_checker_end(&a, &b));
}

int	main(int argc, char *argv[])
{
	if (argc >= 2)
	{
		return (checker(argc, argv, parse_opts(argc, argv)));
	}
	return (error_sys_put(EINVAL));
}
