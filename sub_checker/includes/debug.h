/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   debug.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/15 12:24:17 by lorenuar          #+#    #+#             */
/*   Updated: 2021/04/04 00:46:25 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DEBUG_H
# define DEBUG_H

# include "checker.h"

# include <debug_utils.h>

# define D_STACK(name) _BR(0) _print_stack(name, #name );

static inline void	_print_stack(t_stack *stack, const char *name)
{
	size_t	i;
	size_t	size;

	size = stack_get_size(stack);
	printf("\n=== =START= STACK \"%s\" [%p] === > len %ld <\n", name, stack, size);
	i = 0;
	while (stack)
	{
		printf(
			"[%04ld] NODE [%14p] V data V\n"
			"       |--->[%14p] > %4ld < \n",
			i++, stack, stack->next, stack->data);
		stack = stack->next;
	}
	printf("=== =END= > len %ld <\n", size);
}

#endif
